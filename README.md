```bash
docker run -p 8779:3306 --name members-mysql -e MYSQL_ROOT_PASSWORD=root_pwd -e MYSQL_USER=members_user -e MYSQL_PASSWORD=members_pwd -e MYSQL_DATABASE=members_db -d mysql:5.7
```
The database will be persistent and therefore data will be saved between run.

This persistent also applies to the Flyway migration, therefore if there are changes during development the are not merged there might be a conflict.
In order to restore from the conflict the docker container need to be totally cleaned to initial state.

remove mode ONLY_FULL_GROUP_BY
```sqlite-psql
SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
```

```bash
docker stop members-mysql
docker rm members-mysql
```
