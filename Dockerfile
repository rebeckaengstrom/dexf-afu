FROM openjdk:11-jre

RUN mkdir "/opt/dexf"

COPY "./target/members*.jar" "/opt/dexf/members.jar"
COPY "./docker/*" "/opt/dexf/"

CMD ["-jar", "/opt/dexf/members.jar"]
ENTRYPOINT ["java"]

EXPOSE 8080
EXPOSE 8999
