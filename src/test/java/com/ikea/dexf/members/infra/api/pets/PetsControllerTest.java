package com.ikea.dexf.members.infra.api.pets;

import com.ikea.dexf.members.app.MembersApplication;
import com.ikea.dexf.service.test.database.MySqlTestDatabase;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static io.restassured.RestAssured.basic;
import static io.restassured.RestAssured.given;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        classes = MembersApplication.class)
@TestPropertySource(locations = "classpath:applicationtest.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class PetsControllerTest {

    @LocalServerPort
    private int port;

    @BeforeClass
    public static void beforeClass() {
        MySqlTestDatabase.setupDatabaseBeforeClass();
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        RestAssured.authentication = basic("admin", "nimda");
    }

    @Before
    public void setUp() throws Exception {
        RestAssured.port = port;
    }

    @AfterClass
    public static void afterClass() {
        MySqlTestDatabase.tearDownDatabaseAfterClass();
        RestAssured.reset();
    }

    @Test
    public void pingTest() {
        Response r = given()
                .contentType("application/json")
                .expect().statusCode(HttpStatus.OK.value())
                .when().get("/members/pets/ping");
        r.prettyPrint();
        Assert.assertEquals("pong", r.jsonPath().getString("data"));
    }
}
