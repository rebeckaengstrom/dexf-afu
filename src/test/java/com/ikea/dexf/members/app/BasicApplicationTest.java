package com.ikea.dexf.members.app;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.actuate.autoconfigure.web.server.LocalManagementPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import com.ikea.dexf.service.test.database.MySqlTestDatabase;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
    classes = MembersApplication.class)
@TestPropertySource(locations = "classpath:applicationtest.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class BasicApplicationTest {

  @LocalServerPort
  private int serverPort;

  @LocalManagementPort
  private int managementPort;

  @BeforeClass
  public static void beforeClass() {
    MySqlTestDatabase.setupDatabaseBeforeClass();
    RestAssured.reset();
    RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
  }

  @Before
  public void setUp() throws Exception {
    RestAssured.port = managementPort;
  }

  @AfterClass
  public static void afterClass() {
    MySqlTestDatabase.tearDownDatabaseAfterClass();
    RestAssured.reset();
  }

  @Test
  public void publicuser_management_access() {
    given().expect().statusCode(HttpStatus.OK.value()).when().get("/members/management/health");
  }

  @Test
  public void healthy() {
    JsonPath jsonPath = given().expect().statusCode(HttpStatus.OK.value()).when()
        .get("/members/management/health").jsonPath();

    assertEquals("UP", jsonPath.getString("status"));
  }

  @Test
  public void unauth_get_members() {
    given().expect().statusCode(HttpStatus.UNAUTHORIZED.value()).when().get("/members/Members");
  }

  @Test
  public void basicAuthenticationTest() {

    RestAssured.baseURI = "http://localhost:8079";

    Response response = null;

    String invalidusername = "deepak";
    String invalidpassword = "";

    String validusername = "admin";
    String validpassword = "nimda";


    // Scenario with incorrect username & password
    response =
        given().auth().basic(invalidusername, invalidpassword).when().get("/members/Members");


    System.out.println("Access Unauthorized \nStatus Code :" + response.getStatusCode());
    response.prettyPrint();


    System.out.println("\n---------------------------------------------------\n");

    // Scenario with correct username & password
    response = given().auth().basic(validusername, validpassword).when().get("/members/Members");

    System.out.println("Access Authorized \nStatus Code :" + response.getStatusCode());
    response.prettyPrint();
  }

}
