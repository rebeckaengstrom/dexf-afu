package com.ikea.dexf.members.mock;

import java.io.IOException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = {"application/json"})
public class DummyMock {

  @GetMapping(value = "mock/dummy/get/{something}")
  @ResponseStatus(value = HttpStatus.OK)
  public String updateProduct(
      @PathVariable(value = "something") String something) throws IOException {

    return "{\"data\":\"OK\"}";
  }
}
