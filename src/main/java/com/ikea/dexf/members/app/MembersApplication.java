package com.ikea.dexf.members.app;

import org.springframework.context.annotation.ComponentScan;

//Skannar alla packages med com.ikea.dexf
@ComponentScan(basePackages = "com.ikea.dexf.members")
public class MembersApplication {

}
