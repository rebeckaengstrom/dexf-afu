package com.ikea.dexf.members.app;

import ch.qos.logback.access.tomcat.LogbackValve;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

	@Bean
	public ServletWebServerFactory servletContainerFactory() {
		TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
		// Write access log to console.
		// Configuration is taken from file logback-access.xml in
		// src/main/resources/conf
		factory.addContextValves(new LogbackValve());
		return factory;
	}
}
