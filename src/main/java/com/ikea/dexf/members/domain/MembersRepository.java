package com.ikea.dexf.members.domain;

import java.util.Collection;

public interface MembersRepository {
  // Dessa metoder ska jobba mot databasen.
  public abstract void addMember(Member m);

  public abstract Member getMember(int personId);

  public abstract void updateName(int personId, String newName);

  public abstract void deleteMember(int personId);

  public abstract Collection<Member> getMembers();
}
