package com.ikea.dexf.members.domain;

import java.util.Collection;

public interface MemberService {

  public abstract Member createMember(int personId, String name);

  public abstract Member getMember(int personId);

  public abstract void updateName(int personId, String newName);

  public abstract void deleteMember(int personId);

  public abstract Collection<Member> getMembers();
}
