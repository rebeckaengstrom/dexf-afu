package com.ikea.dexf.members.domain;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.annotation.JsonProperty;

@Service
public class MemberServiceImplementation implements MemberService {
  @JsonProperty("MembersList")
  private static Map<Integer, Member> membersList = new HashMap<>();

  /*
   * static {
   * 
   * Members m1 = new Members(); m1.setName("Johan"); m1.setPersonId(870517);
   * membersList.put(m1.getPersonId(), m1); }
   */

  public Member createMember(int personId, String name) {
    Member m = new Member(personId, name);
    // membersList.put(personId, m);
    return m;
  }

  @Override
  public Member getMember(int personId) {
    Member m = membersList.get(personId);
    return m;
  }

  @Override
  public void updateName(int personId, String newName) {
    Member m = membersList.get(personId);
    m.setName(newName);
    membersList.put(personId, m);

  }

  @Override
  public void deleteMember(int personId) {
    membersList.remove(personId);
  }

  @Override
  public Collection<Member> getMembers() {
    return membersList.values();
  }

}
