package com.ikea.dexf.members.domain;

public class Member {

  private int personId;
  private String name;

  // Ska vi lägga till typer av medlemskap? Guldmedlem osv
  // Hur länge medlemskapet gäller?
  // Kostnad..
  public Member(int personId, String name) {
    this.personId = personId;
    this.name = name;
  }

  public int getPersonId() {
    return personId;
  }

  public void setPersonId(int personId) {
    this.personId = personId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
