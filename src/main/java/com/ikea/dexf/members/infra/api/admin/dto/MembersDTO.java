package com.ikea.dexf.members.infra.api.admin.dto;

import java.util.List;
import java.util.stream.Collectors;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ikea.dexf.members.domain.Member;


public class MembersDTO {
  @JsonProperty("Members")
  public final List<MemberDTO> members;


  public MembersDTO(@JsonProperty("Members") List<MemberDTO> members) {
    this.members = members;
  }

  public List<MemberDTO> getMembers() {
    return members;
  }

  public static MembersDTO from(List<Member> memberList) {
    return new MembersDTO(
        memberList.stream().map(member -> MemberDTO.from(member)).collect(Collectors.toList()));
  }
}
