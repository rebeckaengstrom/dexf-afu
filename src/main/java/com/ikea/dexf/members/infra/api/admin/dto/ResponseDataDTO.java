package com.ikea.dexf.members.infra.api.admin.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseDataDTO {

  @JsonProperty("data")
  String data;

  public ResponseDataDTO() {
  }

  public ResponseDataDTO(String data) {
    this.data = data;
  }

  public String getData() {
    return data;
  }
}
