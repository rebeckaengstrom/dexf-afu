package com.ikea.dexf.members.infra.db;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import com.ikea.dexf.members.domain.Member;
import com.ikea.dexf.members.domain.MembersRepository;

@Repository
public class DBMembersRepository implements MembersRepository {

  // private static final Logger logger = LoggerFactory.getLogger(DBMembersRepository.class);

  private NamedParameterJdbcTemplate readWriteJdbc;
  private NamedParameterJdbcTemplate readOnlyJdbc;


  public DBMembersRepository() {}

  @Autowired
  @Qualifier("readWriteNamedParameterJdbcTemplate")
  public void setReadWriteJdbc(NamedParameterJdbcTemplate readWriteJdbc) {
    this.readWriteJdbc = readWriteJdbc;
  }

  @Autowired
  @Qualifier("readOnlyNamedParameterJdbcTemplate")
  public void setReadOnlyJdbc(NamedParameterJdbcTemplate readOnlyJdbc) {
    this.readOnlyJdbc = readOnlyJdbc;
  }

  @Override
  public void addMember(Member m) {

    var params = new MapSqlParameterSource().addValue("personId", m.getPersonId()).addValue("name",
            m.getName());

    String sql = "INSERT INTO members (personId,name)" + " VALUES (:personId, :name)";
    readWriteJdbc.update(sql, params);

  }

  @Override
  public Member getMember(int personId) {
    // TODO Auto-generated method stub
    RowMapper<Member> rowMapper = (rs, rowNumber) -> new Member(rs.getInt("personId"), rs.getString("name"));
    List<Member> res = readWriteJdbc.query(
            "SELECT * FROM members WHERE personId = :personId LIMIT 1",
            new MapSqlParameterSource("personId", personId),
            rowMapper);
    return !res.isEmpty() ? res.get(0) : null;
  }

  @Override
  public void updateName(int personId, String newName) {
    // TODO Auto-generated method stub

  }

  @Override
  public void deleteMember(int personId) {
    // TODO Auto-generated method stub

  }

  @Override
  public Collection<Member> getMembers() {
    // TODO Auto-generated method stub
    RowMapper<Member> rowMapper = (rs, rowNumber) -> new Member(rs.getInt("personId"), rs.getString("name"));
    return readWriteJdbc.query("SELECT * FROM members", rowMapper);
  }

}