package com.ikea.dexf.members.infra.api.admin.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ikea.dexf.members.domain.Member;


public class MemberDTO {
  @JsonProperty("PersonId")
  private int personId;
  @JsonProperty("Name")
  private String name;

  public MemberDTO(int personId, String name) {
    this.personId = personId;
    this.name = name;
  }

  public int getPersonId() {
    return personId;
  }

  public void setPersonId(int personId) {
    this.personId = personId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public static MemberDTO from(Member mem) {

    return new MemberDTO(mem.getPersonId(), mem.getName());
  }

  public Member toMember() {

    return new Member(this.getPersonId(), this.getName());
  }
}
