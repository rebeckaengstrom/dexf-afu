package com.ikea.dexf.members.infra.api.pets;

import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/pets", produces = "application/json")
public class PetsApiController {

    public PetsApiController() {}

    @RequestMapping(method = RequestMethod.GET, value="ping")
    public ResponseEntity<String> ping() {
        return ResponseEntity
                .ok()
                .cacheControl(CacheControl.noStore())
                .body("{\"data\":\"pong\"}");
    }
}
