package com.ikea.dexf.members.infra.db;

import javax.sql.DataSource;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@Configuration
public class DBConfig {
  @Bean
  @Primary
  @ConfigurationProperties("spring.datasource.read-write")
  public DataSourceProperties readWriteDataSourceProperties() {
    return new DataSourceProperties();
  }

  @Bean
  @Primary
  @ConfigurationProperties("spring.datasource.read-write")
  public DataSource readWriteDataSource() {
    return readWriteDataSourceProperties().initializeDataSourceBuilder().build();
  }

  @Bean
  @ConfigurationProperties("spring.datasource.read-only")
  public DataSourceProperties readOnlyDataSourceProperties() {
    return new DataSourceProperties();
  }

  @Bean
  @ConfigurationProperties("spring.datasource.read-only")
  public DataSource readOnlyDataSource() {
    return readOnlyDataSourceProperties().initializeDataSourceBuilder().build();
  }

  @Bean(name = "readWriteNamedParameterJdbcTemplate")
  @Primary
  public NamedParameterJdbcTemplate readWriteNamedParameterJdbcTemplate() {
    return new NamedParameterJdbcTemplate(readWriteDataSource());
  }

  @Bean(name = "readOnlyNamedParameterJdbcTemplate")
  public NamedParameterJdbcTemplate readOnlyNamedParameterJdbcTemplate() {
    return new NamedParameterJdbcTemplate(readOnlyDataSource());
  }

}