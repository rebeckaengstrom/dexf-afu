package com.ikea.dexf.members.infra.api.admin;

import com.ikea.dexf.members.domain.MembersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.ikea.dexf.members.infra.api.admin.dto.MemberDTO;
import com.ikea.dexf.members.infra.api.admin.dto.ResponseDataDTO;
import com.ikea.dexf.members.infra.db.DBMembersRepository;
import com.ikea.dexf.members.domain.MembersRepository;

@RestController
// @RequestMapping(value = "admin", produces = {"application/json"})
public class AdminApiController {

  //  @Autowired
//  DBMembersRepository memberService;
//  @Autowired
//  MembersRepository memberService;
  private MembersRepository memberService;
  public AdminApiController(MembersRepository memberService) {
    this.memberService = memberService;
  }

  @RequestMapping(method = RequestMethod.GET, value = "ping")
  public ResponseDataDTO ping() {
    return new ResponseDataDTO("pong");
  }

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public ResponseDataDTO test() {
    return new ResponseDataDTO("Hallåhej!");
  }

  @RequestMapping(value = "/addmember", method = RequestMethod.POST)
  public void createBody(@RequestBody MemberDTO mem) {
    memberService.addMember(mem.toMember());
  }

  @RequestMapping(value = "/Members/{personId}", method = RequestMethod.GET)
  public ResponseEntity<Object> getMember(@PathVariable int personId) {
    return new ResponseEntity<>(memberService.getMember(personId), HttpStatus.OK);
  }

  @RequestMapping(value = "/Members")
  public ResponseEntity<Object> getMembers() {
    return new ResponseEntity<>(memberService.getMembers(), HttpStatus.OK);
  }



}