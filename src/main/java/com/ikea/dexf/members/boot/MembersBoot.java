package com.ikea.dexf.members.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.ikea.dexf.members.app.MembersApplication;

@SpringBootApplication
public class MembersBoot {

  public static void main(String[] args) throws Exception {
    new SpringApplication(MembersApplication.class).run(args);
  }
}
